/**
 * 线性表：顺序表、链表
 * 主要包括以下内容：
 * 1. 顺序表的定义、初始化、插入、删除、查找、遍历等操作
 */
#include <iostream>
using namespace std;
// 顺序表定义
#define MAXSIZE 100// 定义最大长度
typedef struct {
    int data[MAXSIZE];// 用数组存储数据元素
    int length;       // 顺序表的当前长度
} SqList;

// 初始化顺序表
void InitList(SqList &L) {
    L.length = 0;
}

// 插入元素
bool ListInsert(SqList &L, int i, int e) {
    if (i < 1 || i > L.length + 1) {
        return false;// i值不合法
    }
    if (L.length >= MAXSIZE) {
        return false;// 当前存储空间已满，不能插入
    }
    for (int j = L.length; j >= i; j--) {
        L.data[j] = L.data[j - 1];// 将第i个位置及之后的元素后移
    }
    L.data[i - 1] = e;// 在位置i处放入e
    L.length++;       // 长度加1
    return true;
}

// 删除元素
bool ListDelete(SqList &L, int i) {
    if (i < 1 || i > L.length) {
        return false;// i值不合法
    }
    for (int j = i; j < L.length; j++) {
        L.data[j - 1] = L.data[j];// 将第i个位置之后的元素前移
    }
    L.length--;// 长度减1
    return true;
}

// 查找元素
int LocateElem(SqList L, int e) {
    for (int i = 0; i < L.length; i++) {
        if (L.data[i] == e) {
            return i + 1;// 返回元素在线性表中的位置
        }
    }
    return 0;// 未找到
}

// 遍历顺序表
void TraverseList(SqList L) {
    for (int i = 0; i < L.length; i++) {
        cout << L.data[i] << "\t";
    }
    cout << endl;
}

int main() {
    SqList L;
    InitList(L);
    int arr[10] = {2, 34, 5, 32, 76, 23, 99, 83, 21, 23};
    for (int i = 0; i < 10; ++i) {
        cout << i << "\t";
        ListInsert(L, 1, arr[i]);
    }
    cout << "\n线性表长度：" << L.length << endl;
    TraverseList(L);
}